This is the PetriNetModeler project containing exactly those features as used
in the STTT article [CINCO: A Simplicity-Driven Approach to Full Generation of
Domain-Specific Graphical Modeling
Tools](http://dx.doi.org/10.1007/s10009-017-0453-6) (aka "Cinco primer").
However, it might undergo minor changes, if the syntax of Cinco's specification
files changes in the future. While the article used Cinco 0.7 syntax, this project currently is updated to Cinco 1.0 syntax.

For a more complex example regarding Petri nets, refer to [../petrinet/](https://gitlab.com/scce/cinco-examples/petrinet)
